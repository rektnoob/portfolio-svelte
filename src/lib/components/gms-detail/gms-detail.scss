@use '/static/assets/sass/abstracts/colors';
@use 'sass:math';

section.gms-detail {
  display: flex;
  height: 100%;
  align-items: center;

  section.content {
    container-type: inline-size;
    background-color: colors.$primary01;
    display: flex;
    flex-direction: column;
    aspect-ratio: math.div(16, 9);
    border-radius: 5px;
    width: 100%;
    margin: 0 0.5rem;

    header {
      display: flex;
      flex-direction: column;
      align-items: center;
      h3 {
        color: colors.$accent01;
        margin: 4rem 0 0;
      }
      h4 {
        color: colors.$lightest;
        margin: 0.5rem 0;
      }
    }
    section.details {
      display: flex;
      flex-direction: column;
      justify-content: space-evenly;
      align-items: center;
      height: 75%;

      aside.summary {
        color: colors.$lightest;
        text-align: center;
        h6 {
          margin: 1rem 0;
        }
      }

      aside {
        &.icons1,
        &.icons2 {
          display: flex;
          justify-content: space-between;
          section.icon-wrapper {
            display: flex;
            justify-content: center;
          }
        }
      }

      aside.icons1 {
        margin: 0.75rem 0;
        width: 70%;
        section.icon-wrapper {
          width: 6.75rem;
        }
      }
      aside.icons2 {
        width: 45%;
        section.icon-wrapper {
          width: 5.75rem;
        }
      }
      aside.icon-desc {
        color: colors.$secondary03;
        p {
          font-size: 0.75rem;
        }
      }
    }
  }
}

@mixin remove() {
  visibility: hidden;
  height: 0;
  width: 0;
}

@container (max-width: 750px) {
  section.gms-detail section.content {
    section.details aside.summary {
      @include remove();
    }
  }
}

@container (max-width: 425px) {
  section.gms-detail section.content {
    header {
      @include remove();
    }
    section.details {
      height: 12rem;
      aside.icons1,
      aside.icons2 {
        flex-direction: row;
        transform: scale(0.75);
        width: auto;
        margin: 0;
      }
    }
  }
}
